export default class GamePlayScreen extends Phaser.Scene {
    constructor() {
        super();
    }

    preload() {
        this.load.image("player", "assets/playerShip2_green.png");
    }

    create(){
        this.add.image(56, 200, "player");
    }
}
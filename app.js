import GamePlayScreen from "./scenes/GamePlayScreen.js";

var config = {
  type: Phaser.AUTO,
  width: window.innerWidth,
  height: window.innerHeight,
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 200 },
    },
  },
  scene: [GamePlayScreen],

  mode: Phaser.Scale.FIT,
};

var game = new Phaser.Game(config);

function preload() {
  //   this.load.image("player", "assets/playerShip2_green.png");
}

function create() {}
